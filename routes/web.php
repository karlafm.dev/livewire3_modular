<?php

use App\Livewire\Posts\CreatePost;
use Illuminate\Support\Facades\Route;
use App\Livewire\Asuntos\ListarPersonasComponent;
use App\Livewire\Asuntos\RegistrarAsuntoSeccionGeneralComponent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/posts', CreatePost::class);

Route::get('/asuntos/{id_asunto?}', RegistrarAsuntoSeccionGeneralComponent::class);
Route::get('/personas', ListarPersonasComponent::class);
Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
