INSERT INTO tipo_respuesta(id_tipo_respuesta, nombre, activo) OVERRIDING SYSTEM VALUE VALUES  (1,'Archivo','true');
INSERT INTO tipo_respuesta(id_tipo_respuesta, nombre, activo) OVERRIDING SYSTEM VALUE VALUES  (2,'Biblioteca','true');
INSERT INTO tipo_respuesta(id_tipo_respuesta, nombre, activo) OVERRIDING SYSTEM VALUE VALUES  (3,'Enviado a Secretario Particular','true');
INSERT INTO tipo_respuesta(id_tipo_respuesta, nombre, activo) OVERRIDING SYSTEM VALUE VALUES  (4,'Enviado a Secretario Privado','true');
INSERT INTO tipo_respuesta(id_tipo_respuesta, nombre, activo) OVERRIDING SYSTEM VALUE VALUES  (5,'Pendiente','true');
INSERT INTO tipo_respuesta(id_tipo_respuesta, nombre, activo) OVERRIDING SYSTEM VALUE VALUES  (6,'Sin Respuesta','true');
INSERT INTO tipo_respuesta(id_tipo_respuesta, nombre, activo) OVERRIDING SYSTEM VALUE VALUES  (7,'Turnado por oficio','true');
INSERT INTO tipo_respuesta(id_tipo_respuesta, nombre, activo) OVERRIDING SYSTEM VALUE VALUES  (8,'Turnado por oficio por acuerdo del Rector','true');
INSERT INTO tipo_respuesta(id_tipo_respuesta, nombre, activo) OVERRIDING SYSTEM VALUE VALUES  (9,'Turnado por volante','true');
