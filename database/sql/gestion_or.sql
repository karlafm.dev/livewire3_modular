
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET client_min_messages = warning;
SET row_security = off;

CREATE TABLE public.accion (
    id_accion integer NOT NULL,
    nombre character varying(50) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);

COMMENT ON TABLE public.accion IS 'Catálogo de acciones realizadas por un usuario en el sistema.';
COMMENT ON COLUMN public.accion.id_accion IS 'Identificador de la acción realizada por un usuario en el sistema, por ejemplo: 7.';
COMMENT ON COLUMN public.accion.nombre IS 'Nombre de la acción que se almacenará en la bitácora.';
COMMENT ON COLUMN public.accion.created_at IS 'Fecha y hora de creación del registro.';
COMMENT ON COLUMN public.accion.updated_at IS 'Fecha y hora de última modificación del registro.';

ALTER TABLE public.accion ALTER COLUMN id_accion ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.accion_id_accion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE public.area_universitaria_organizacion (
    id_area_universitaria_organizacion integer NOT NULL,
    nombre character varying(250) NOT NULL,
	activo boolean NOT NULL DEFAULT true,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);


COMMENT ON TABLE public.area_universitaria_organizacion IS 'Área universitaria, organización o institución.';

COMMENT ON COLUMN public.area_universitaria_organizacion.id_area_universitaria_organizacion IS 'Identificador del área universitaria o empresa.';

COMMENT ON COLUMN public.area_universitaria_organizacion.nombre IS 'Nombre del área universitaria, empresa, organización o institución.';

COMMENT ON COLUMN public.area_universitaria_organizacion.activo IS 'Indica si el área se encuentra Activa = true, Inactiva = false.';

COMMENT ON COLUMN public.area_universitaria_organizacion.created_at IS 'Fecha y hora de creación del registro.';

COMMENT ON COLUMN public.area_universitaria_organizacion.updated_at IS 'Fecha y hora de última modificación del registro.';

ALTER TABLE public.area_universitaria_organizacion ALTER COLUMN id_area_universitaria_organizacion ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.area_universitaria_id_area_universitaria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE public.asunto (
    id_asunto integer NOT NULL,
    id_estado integer NOT NULL,
    id_usuario integer,
    id_gestion_rector integer,
    folio character varying(8),
    asunto character varying(7000) NOT NULL,
    detalle text,
    fecha_recepcion date NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);

COMMENT ON TABLE public.asunto IS 'Registro de los asuntos recibidos en la Oficina de Rectoría.';
COMMENT ON COLUMN public.asunto.id_asunto IS 'Identificador del asunto, por ejemplo: 10';

COMMENT ON COLUMN public.asunto.id_estado IS 'Identificador del estado del asunto. Por ejemplo: 4.';
COMMENT ON COLUMN public.asunto.id_usuario IS 'Identificador del usuario que registra el asunto.';

COMMENT ON COLUMN public.asunto.id_gestion_rector IS 'Identificador del periodo de gestión del rectorado en el cual se registra el asunto.';
COMMENT ON COLUMN public.asunto.folio IS 'Número de folio del asunto por gestión del rector.';

COMMENT ON COLUMN public.asunto.asunto IS 'Nombre corto del asunto.';
COMMENT ON COLUMN public.asunto.detalle IS 'Descripción detallada del asunto.';

COMMENT ON COLUMN public.asunto.fecha_recepcion IS 'Fecha de recepción del asunto.';

COMMENT ON COLUMN public.asunto.created_at IS 'Fecha y hora de creación del registro.';

COMMENT ON COLUMN public.asunto.updated_at IS 'Fecha y hora de última modificación del registro.';

CREATE TABLE public.asunto_area_universitaria (
    id_asunto_area_universitaria integer NOT NULL,
    id_asunto integer NOT NULL,
    id_area_universitaria_organizacion integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);


COMMENT ON TABLE public.asunto_area_universitaria IS 'Nombres de las áreas universitarias, instituciones, empresas u organizaciones a las que pertenecen las personas que solicitan la atención de un asunto recibido en la Oficina de Rectoría.';

COMMENT ON COLUMN public.asunto_area_universitaria.id_asunto_area_universitaria IS 'Identificador consecutivo del asunto y la persona.';

COMMENT ON COLUMN public.asunto_area_universitaria.id_asunto IS 'Identificador consecutivo del asunto.';

COMMENT ON COLUMN public.asunto_area_universitaria.id_area_universitaria_organizacion IS 'Identificador de la empresa o área universitaria que solicita la atención de un asunto.';

COMMENT ON COLUMN public.asunto_area_universitaria.created_at IS 'Fecha y hora de creación del registro.';

COMMENT ON COLUMN public.asunto_area_universitaria.updated_at IS 'Fecha y hora de última modificación del registro.';

ALTER TABLE public.asunto_area_universitaria ALTER COLUMN id_asunto_area_universitaria ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.asunto_area_universitaria_id_asunto_area_universitaria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE public.asunto_persona (
    id_asunto_persona integer NOT NULL,
    id_asunto integer NOT NULL,
    nombre character varying(150) NOT NULL,
    figura character varying(150) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);


COMMENT ON TABLE public.asunto_persona IS 'Nombres de las personas que solicitan la atención de un asunto recibido en la Oficina de Rectoría.';

COMMENT ON COLUMN public.asunto_persona.id_asunto_persona IS 'Identificador consecutivo del asunto y la persona.';
COMMENT ON COLUMN public.asunto_persona.id_asunto IS 'Identificador consecutivo del asunto.';
COMMENT ON COLUMN public.asunto_persona.nombre IS 'Nombre de la persona que solicita la atención de un asunto.';
COMMENT ON COLUMN public.asunto_persona.figura IS 'Indica el tipo de persona que solicita el seguimiento de un asunto. Por ejemplo: ALUMNO.';
COMMENT ON COLUMN public.asunto_persona.created_at IS 'Fecha y hora de creación del registro.';
COMMENT ON COLUMN public.asunto_persona.updated_at IS 'Fecha y hora de última modificación del registro.';


ALTER TABLE public.asunto_persona ALTER COLUMN id_asunto_persona ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.asunto_persona_id_asunto_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 237 (class 1259 OID 295154)
-- Name: asunto_relacionado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.asunto_relacionado (
    id_asunto integer NOT NULL,
    id_asunto_relacionado integer NOT NULL,
    id_usuario integer,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);


COMMENT ON TABLE public.asunto_relacionado IS 'Registro de los asuntos relacionados por el mismo tema.';


--
-- TOC entry 3516 (class 0 OID 0)
-- Dependencies: 237
-- Name: COLUMN asunto_relacionado.id_asunto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_relacionado.id_asunto IS 'Identificador del asunto a relacionar, por ejemplo: 1';


--
-- TOC entry 3517 (class 0 OID 0)
-- Dependencies: 237
-- Name: COLUMN asunto_relacionado.id_asunto_relacionado; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_relacionado.id_asunto_relacionado IS 'Identificador del asunto relacionado, por ejemplo: 10';


--
-- TOC entry 3518 (class 0 OID 0)
-- Dependencies: 237
-- Name: COLUMN asunto_relacionado.id_usuario; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_relacionado.id_usuario IS 'Identificador del usuario que registra el asunto.';


--
-- TOC entry 3519 (class 0 OID 0)
-- Dependencies: 237
-- Name: COLUMN asunto_relacionado.created_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_relacionado.created_at IS 'Fecha y hora de creación del registro.';


--
-- TOC entry 3520 (class 0 OID 0)
-- Dependencies: 237
-- Name: COLUMN asunto_relacionado.updated_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_relacionado.updated_at IS 'Fecha y hora de última modificación del registro.';


--
-- TOC entry 213 (class 1259 OID 294672)
-- Name: bitacora; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bitacora (
    id_bitacora integer NOT NULL,
    id_accion integer NOT NULL,
    descripcion text,
    fecha_hora timestamp without time zone DEFAULT now() NOT NULL,
    nota text,
    registro_tipo character varying(30),
    registro_id integer,
    id_usuario integer
);

COMMENT ON TABLE public.bitacora IS 'Bitácora de las acciones realizadas por los usuarios en el sistema.';


--
-- TOC entry 3522 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN bitacora.id_bitacora; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.bitacora.id_bitacora IS 'Identificador del registro en la bitácora, por ejemplo: 1.';


--
-- TOC entry 3523 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN bitacora.id_accion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.bitacora.id_accion IS 'Identificador de la acción realizada en el sistema, por ejemplo: 10.';


--
-- TOC entry 3524 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN bitacora.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.bitacora.descripcion IS 'Descripción de la acción realizada en el sistema, por ejemplo: iniciar sesión.';


--
-- TOC entry 3525 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN bitacora.fecha_hora; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.bitacora.fecha_hora IS 'Fecha y hora del registro de la acción en la bitácora.';


--
-- TOC entry 3526 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN bitacora.nota; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.bitacora.nota IS 'Notas adicionales de las acciones registradas en la bitácora que aportan mayor detalle de la acción realizada.';


--
-- TOC entry 3527 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN bitacora.registro_tipo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.bitacora.registro_tipo IS 'Tipo de registro, por ejemplo: registro asunto.';


--
-- TOC entry 3528 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN bitacora.registro_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.bitacora.registro_id IS 'Identificador del tipo de registro: por ejemplo: 25.';


--
-- TOC entry 3529 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN bitacora.id_usuario; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.bitacora.id_usuario IS 'Identificador del usuario.';


--
-- TOC entry 214 (class 1259 OID 294678)
-- Name: bitacora_id_bitacora_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.bitacora ALTER COLUMN id_bitacora ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.bitacora_id_bitacora_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 228 (class 1259 OID 295031)
-- Name: asunto_documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.asunto_documento (
    id_asunto_documento integer NOT NULL,
    id_asunto integer NOT NULL,
    id_usuario integer,
	id_tipo_documento integer,
    ruta_archivo character varying(250) NOT NULL,
    nombre character varying(250),
    identificador character varying(50),
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);

COMMENT ON COLUMN public.asunto_documento.id_tipo_documento IS 'Identificador del tipo de documento con el cual se registra el asunto.';
COMMENT ON TABLE public.asunto_documento IS 'Documentos relacionados con los asuntos.';


--
-- TOC entry 3531 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN asunto_documento.id_asunto_documento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_documento.id_asunto_documento IS 'Identificador del documento adjunto al asunto, por ejemplo: 6.';


--
-- TOC entry 3532 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN asunto_documento.id_asunto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_documento.id_asunto IS 'Identificador del asunto, por ejemplo: 5.';


--
-- TOC entry 3533 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN asunto_documento.id_usuario; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_documento.id_usuario IS 'Identificador del usuario, por ejemplo: 120.';


--
-- TOC entry 3534 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN asunto_documento.ruta_archivo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_documento.ruta_archivo IS 'Ruta donde se almacena el archivo.';


--
-- TOC entry 3535 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN asunto_documento.nombre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_documento.nombre IS 'Nombre descriptivo del archivo. Por ejemplo: oficio.';


--
-- TOC entry 3536 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN asunto_documento.identificador; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_documento.identificador IS 'Identificar del documento cuando lo tenga, por ejemplo el número de oficio o volante: SPASU/DGAPSU/090/23.';


--
-- TOC entry 3537 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN asunto_documento.created_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_documento.created_at IS 'Fecha y hora de creación del registro.';


--
-- TOC entry 3538 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN asunto_documento.updated_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.asunto_documento.updated_at IS 'Fecha y hora de última modificación del registro.';


--
-- TOC entry 227 (class 1259 OID 295030)
-- Name: documento_id_documento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.asunto_documento ALTER COLUMN id_asunto_documento ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.asunto_documento_id_asunto_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 236 (class 1259 OID 295141)
-- Name: respuesta_documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.respuesta_documento (
    id_respuesta_documento integer NOT NULL,
    id_respuesta integer,
    id_usuario integer,
    ruta_archivo character varying(250) NOT NULL,
    nombre character varying(250),
    identificador character varying(50),
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);

COMMENT ON TABLE public.respuesta_documento IS 'Documentos relacionados con los asuntos.';


--
-- TOC entry 3540 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN respuesta_documento.id_respuesta_documento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.respuesta_documento.id_respuesta_documento IS 'Identificador del documento adjunto al asunto, por ejemplo: 6.';


--
-- TOC entry 3541 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN respuesta_documento.id_respuesta; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.respuesta_documento.id_respuesta IS 'Identificador de la respuesta del asunto, por ejemplo: 2.';


--
-- TOC entry 3542 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN respuesta_documento.id_usuario; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.respuesta_documento.id_usuario IS 'Identificador del usuario, por ejemplo: 120.';


--
-- TOC entry 3543 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN respuesta_documento.ruta_archivo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.respuesta_documento.ruta_archivo IS 'Ruta donde se almacena el archivo.';


--
-- TOC entry 3544 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN respuesta_documento.nombre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.respuesta_documento.nombre IS 'Nombre descriptivo del archivo. Por ejemplo: oficio.';


--
-- TOC entry 3545 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN respuesta_documento.identificador; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.respuesta_documento.identificador IS 'Identificar del documento cuando lo tenga, por ejemplo el número de oficio o volante: SPASU/DGAPSU/090/23.';


--
-- TOC entry 3546 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN respuesta_documento.created_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.respuesta_documento.created_at IS 'Fecha y hora de creación del registro.';


--
-- TOC entry 3547 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN respuesta_documento.updated_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.respuesta_documento.updated_at IS 'Fecha y hora de última modificación del registro.';


--
-- TOC entry 235 (class 1259 OID 295140)
-- Name: respuesta_documento_id_respuesta_documento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.respuesta_documento ALTER COLUMN id_respuesta_documento ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.respuesta_documento_id_respuesta_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 230 (class 1259 OID 295074)
-- Name: estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estado (
    id_estado integer NOT NULL,
    nombre character varying(60) NOT NULL,
	activo boolean NOT NULL default true,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);

COMMENT ON TABLE public.estado IS 'Catálogo de estados de los asuntos recibidos en la Oficina de Rectoría.';
COMMENT ON COLUMN public.estado.id_estado IS 'Identificador del estado del asunto.';
COMMENT ON COLUMN public.estado.activo IS 'Indica si el registro está activo = true o inactivo = false.';


--
-- TOC entry 3550 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN estado.nombre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.estado.nombre IS 'Nombre del estado del asunto. Por ejemplo: Registrado, En atención, Cerrado';


--
-- TOC entry 3551 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN estado.created_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.estado.created_at IS 'Fecha y hora de creación del registro.';


--
-- TOC entry 3552 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN estado.updated_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.estado.updated_at IS 'Fecha y hora de última modificación del registro.';


--
-- TOC entry 229 (class 1259 OID 295073)
-- Name: estado_id_estado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.estado ALTER COLUMN id_estado ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.estado_id_estado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 222 (class 1259 OID 294863)
-- Name: tipo_respuesta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_respuesta (
    id_tipo_respuesta integer NOT NULL,
    nombre character varying(60) NOT NULL,
	activo boolean NOT NULL default true,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);

COMMENT ON TABLE public.tipo_respuesta IS 'Catálogo de los tipos de respuestas de los asuntos recibidos en la Oficina de Rectoría.';
COMMENT ON COLUMN public.tipo_respuesta.id_tipo_respuesta IS 'Identificador del tipo de respuesta del asunto.';
COMMENT ON COLUMN public.tipo_respuesta.activo IS 'Indica si el tipo de respuesta está activo = true o inactivo = false.';
COMMENT ON COLUMN public.tipo_respuesta.nombre IS 'Nombre del tipo de respuesta del asunto. Por ejemplo: Turnado a Secretario particular.';


--
-- TOC entry 3556 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN tipo_respuesta.created_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tipo_respuesta.created_at IS 'Fecha y hora de creación del registro.';


--
-- TOC entry 3557 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN tipo_respuesta.updated_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tipo_respuesta.updated_at IS 'Fecha y hora de última modificación del registro.';

ALTER TABLE public.tipo_respuesta ALTER COLUMN id_tipo_respuesta ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tipo_respuesta_id_tipo_respuesta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 224 (class 1259 OID 294898)
-- Name: expediente; Type: TABLE; Schema: public; Owner: postgres
--

/*
CREATE TABLE public.expediente (
    id_expediente integer NOT NULL,
    id_gestion_rector integer NOT NULL,
    id_usuario integer NOT NULL,
    anio integer NOT NULL,
    tema character varying(500) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);


COMMENT ON TABLE public.expediente IS 'Expedientes de los asuntos recibidos en la Oficina de Rectoría.';
COMMENT ON COLUMN public.expediente.id_expediente IS 'Identificador del expediente de la Oficina de Rectoría.';
COMMENT ON COLUMN public.expediente.id_gestion_rector IS 'Identificador del periodo de gestión del rector.';
COMMENT ON COLUMN public.expediente.id_usuario IS 'Identificador del usuario que registra el expediente.';
COMMENT ON COLUMN public.expediente.anio IS 'Año del expediente.';
COMMENT ON COLUMN public.expediente.tema IS 'Tema general del expediente.';
COMMENT ON COLUMN public.expediente.created_at IS 'Fecha y hora de creación del registro.';
COMMENT ON COLUMN public.expediente.updated_at IS 'Fecha y hora de última modificación del registro.';
ALTER TABLE public.expediente ALTER COLUMN id_expediente ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.expediente_id_expediente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
*/

CREATE TABLE public.gestion_rector (
    id_gestion_rector integer NOT NULL,
    nombre_periodo character varying(60) NOT NULL,
    nombre character varying(50) NOT NULL,
    primer_apellido character varying(50) NOT NULL,
    segundo_apellido character varying(50) NOT NULL,
    periodo smallint NOT NULL,
    vigente boolean NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);

COMMENT ON TABLE public.gestion_rector IS 'Catálogo de periodos de gestión de los rectores de la universidad.';
COMMENT ON COLUMN public.gestion_rector.id_gestion_rector IS 'Identificador consecutivo de la gestión del rector.';
COMMENT ON COLUMN public.gestion_rector.nombre_periodo IS 'Nombre del periodo de la gestión del rector. Por ejemplo: 2019-2023.';

COMMENT ON COLUMN public.gestion_rector.nombre IS 'Nombre del rector. Por ejemplo: Enrique.';


--
-- TOC entry 3570 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN gestion_rector.primer_apellido; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.gestion_rector.primer_apellido IS 'Primer apellido del rector. Por ejemplo: Graue.';


--
-- TOC entry 3571 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN gestion_rector.segundo_apellido; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.gestion_rector.segundo_apellido IS 'Segundo apellido del rector. Por ejemplo: Wiechers.';
COMMENT ON COLUMN public.gestion_rector.periodo IS 'Indica si es su primer o segundo periodo como rector. Por ejemplo: 2.';
COMMENT ON COLUMN public.gestion_rector.vigente IS 'Indica si es el rector vigente = true, anterior = false.';
COMMENT ON COLUMN public.gestion_rector.created_at IS 'Fecha y hora de creación del registro.';
COMMENT ON COLUMN public.gestion_rector.updated_at IS 'Fecha y hora de última modificación del registro.';

ALTER TABLE public.gestion_rector ALTER COLUMN id_gestion_rector ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.gestion_rector_id_gestion_rector_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


CREATE TABLE public.respuesta (
    id_respuesta integer NOT NULL,
    id_tipo_respuesta integer NOT NULL,
    id_asunto integer NOT NULL,
    identificador_respuesta character varying(30),
    observaciones text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);

COMMENT ON TABLE public.respuesta IS 'Respuestas de los asuntos recibidos en la Oficina de Rectoría.';
COMMENT ON COLUMN public.respuesta.id_respuesta IS 'Identificador de la respuesta del asunto.';
COMMENT ON COLUMN public.respuesta.id_tipo_respuesta IS 'Identificador del tipo de respuesta del asunto.';
COMMENT ON COLUMN public.respuesta.id_asunto IS 'Identificador del asunto.';
COMMENT ON COLUMN public.respuesta.identificador_respuesta IS 'Identificador del documento de respuesta del asunto, como el número de oficio. Por ejemplo: ABC/1000/01';
COMMENT ON COLUMN public.respuesta.observaciones IS 'Observaciones de la respuesta del asunto.';
COMMENT ON COLUMN public.respuesta.created_at IS 'Fecha y hora de creación del registro.';
COMMENT ON COLUMN public.respuesta.updated_at IS 'Fecha y hora de última modificación del registro.';


ALTER TABLE public.respuesta ALTER COLUMN id_respuesta ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.respuesta_id_respuesta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 218 (class 1259 OID 294708)
-- Name: tipo_documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_documento (
    id_tipo_documento integer NOT NULL,
    nombre character varying(200) NOT NULL,
    nomenclatura character varying(60),
    activo boolean NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);
COMMENT ON TABLE public.tipo_documento IS 'Catálogo de tipos de documentos que se incluyen por medio del sistema durante el registro y seguimiento de los asuntos.';
COMMENT ON COLUMN public.tipo_documento.id_tipo_documento IS 'Identificador del tipo de documento, por ejemplo:3.';
COMMENT ON COLUMN public.tipo_documento.nombre IS 'Nombre del tipo de documento, por ejemplo: Oficio.';
COMMENT ON COLUMN public.tipo_documento.nomenclatura IS 'Nomenclatura para el nombrado del archivo.';
COMMENT ON COLUMN public.tipo_documento.activo IS 'Estado del tipo de documento, por ejemplo: TRUE - activo, FALSE = inactivo.';


--
-- TOC entry 3589 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN tipo_documento.created_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tipo_documento.created_at IS 'Fecha y hora de creación del registro.';


--
-- TOC entry 3590 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN tipo_documento.updated_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tipo_documento.updated_at IS 'Fecha y hora de última modificación del registro.';


--
-- TOC entry 217 (class 1259 OID 294707)
-- Name: tipo_documento_id_tipo_documento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.tipo_documento ALTER COLUMN id_tipo_documento ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tipo_documento_id_tipo_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 215 (class 1259 OID 294679)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id_usuario integer NOT NULL,
    nombre character varying(50) NOT NULL,
    primer_apellido character varying(50) NOT NULL,
    segundo_apellido character varying(50),
    email character varying(150) NOT NULL,
    password character varying(255) NOT NULL,
    curp character varying(18),
    email_verified_at timestamp without time zone,
    remember_token character varying(100),
    activo boolean DEFAULT true,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);

COMMENT ON TABLE public.usuario IS 'Usuarios que tienen acceso al sistema.';
COMMENT ON COLUMN public.usuario.id_usuario IS 'Identificador del usuario, por ejemplo: 10.';
COMMENT ON COLUMN public.usuario.nombre IS 'Nombre del usuario, por ejemplo: Juan.';
COMMENT ON COLUMN public.usuario.primer_apellido IS 'Primer apellido del usuario, por ejemplo: López.';
COMMENT ON COLUMN public.usuario.segundo_apellido IS 'Segundo apellido del usuario, por ejemplo: Pérez.';
COMMENT ON COLUMN public.usuario.email IS 'Correo electrónico del usuario, por ejemplo: usuario_prueba@unam.mx.';
COMMENT ON COLUMN public.usuario.password IS 'Password cifrado del usuario.';
COMMENT ON COLUMN public.usuario.curp IS 'CURP del usuario.';
COMMENT ON COLUMN public.usuario.email_verified_at IS 'Campo usado por laravel para verificar el email.';
COMMENT ON COLUMN public.usuario.remember_token IS 'Campo usado por laravel para saber si debe recordar el token.';
COMMENT ON COLUMN public.usuario.activo IS 'Indica si el usuario se encuentra activo = true, inactivo = false.';
COMMENT ON COLUMN public.usuario.created_at IS 'Fecha y hora de creación del registro.';
COMMENT ON COLUMN public.usuario.updated_at IS 'Fecha y hora de última modificación del registro.';

ALTER TABLE public.usuario ALTER COLUMN id_usuario ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);



--
-- TOC entry 3604 (class 0 OID 0)
-- Dependencies: 210
-- Name: accion_id_accion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.accion_id_accion_seq', 1, false);


--
-- TOC entry 3605 (class 0 OID 0)
-- Dependencies: 212
-- Name: area_universitaria_id_area_universitaria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.area_universitaria_id_area_universitaria_seq', 1, false);




SELECT pg_catalog.setval('public.asunto_area_universitaria_id_asunto_area_universitaria_seq', 1, false);


--
-- TOC entry 3607 (class 0 OID 0)
-- Dependencies: 225
-- Name: asunto_persona_id_asunto_persona_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.asunto_persona_id_asunto_persona_seq', 1, false);
SELECT pg_catalog.setval('public.bitacora_id_bitacora_seq', 1, false);
SELECT pg_catalog.setval('public.asunto_documento_id_asunto_documento_seq', 1, false);


--
-- TOC entry 3610 (class 0 OID 0)
-- Dependencies: 235
-- Name: respuesta_documento_id_respuesta_documento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.respuesta_documento_id_respuesta_documento_seq', 1, false);


--
-- TOC entry 3611 (class 0 OID 0)
-- Dependencies: 229
-- Name: estado_id_estado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estado_id_estado_seq', 1, false);

SELECT pg_catalog.setval('public.tipo_respuesta_id_tipo_respuesta_seq', 1, false);
SELECT pg_catalog.setval('public.gestion_rector_id_gestion_rector_seq', 1, false);
SELECT pg_catalog.setval('public.respuesta_id_respuesta_seq', 1, false);


--
-- TOC entry 3616 (class 0 OID 0)
-- Dependencies: 217
-- Name: tipo_documento_id_tipo_documento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_documento_id_tipo_documento_seq', 1, false);


--
-- TOC entry 3617 (class 0 OID 0)
-- Dependencies: 216
-- Name: usuario_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_usuario_seq', 1, false);


--
-- TOC entry 3255 (class 2606 OID 294688)
-- Name: accion accion_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accion
    ADD CONSTRAINT accion_pk PRIMARY KEY (id_accion);


--
-- TOC entry 3257 (class 2606 OID 294936)
-- Name: area_universitaria_organizacion area_uk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_universitaria_organizacion
    ADD CONSTRAINT area_uk UNIQUE (nombre);


--
-- TOC entry 3259 (class 2606 OID 294692)
-- Name: area_universitaria_organizacion area_universitaria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_universitaria_organizacion
    ADD CONSTRAINT area_universitaria_pkey PRIMARY KEY (id_area_universitaria_organizacion);


--
-- TOC entry 3267 (class 2606 OID 294861)
-- Name: asunto_area_universitaria asunto_empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asunto_area_universitaria
    ADD CONSTRAINT asunto_empresa_pkey PRIMARY KEY (id_asunto_area_universitaria);


--
-- TOC entry 3273 (class 2606 OID 294955)
-- Name: asunto_persona asunto_persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asunto_persona
    ADD CONSTRAINT asunto_persona_pkey PRIMARY KEY (id_asunto_persona);


--
-- TOC entry 3287 (class 2606 OID 295198)
-- Name: asunto asunto_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asunto
    ADD CONSTRAINT asunto_pk PRIMARY KEY (id_asunto);


--
-- TOC entry 3285 (class 2606 OID 295159)
-- Name: asunto_relacionado asunto_relacionado_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asunto_relacionado
    ADD CONSTRAINT asunto_relacionado_pk PRIMARY KEY (id_asunto, id_asunto_relacionado);


--
-- TOC entry 3261 (class 2606 OID 294694)
-- Name: bitacora bitacora_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bitacora
    ADD CONSTRAINT bitacora_pk PRIMARY KEY (id_bitacora);


--
-- TOC entry 3275 (class 2606 OID 295038)
-- Name: asunto_documento documento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asunto_documento
    ADD CONSTRAINT documento_pk PRIMARY KEY (id_asunto_documento);


--
-- TOC entry 3283 (class 2606 OID 295148)
-- Name: respuesta_documento respuesta_documento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.respuesta_documento
    ADD CONSTRAINT respuesta_documento_pk PRIMARY KEY (id_respuesta_documento);


--
-- TOC entry 3277 (class 2606 OID 295079)
-- Name: estado estado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id_estado);

ALTER TABLE ONLY public.tipo_respuesta
    ADD CONSTRAINT tipo_respuesta_pkey PRIMARY KEY (id_tipo_respuesta);

/*
ALTER TABLE ONLY public.expediente
    ADD CONSTRAINT expediente_pkey PRIMARY KEY (id_expediente);
*/
ALTER TABLE ONLY public.gestion_rector
    ADD CONSTRAINT gestion_rector_pkey PRIMARY KEY (id_gestion_rector);

ALTER TABLE ONLY public.respuesta
    ADD CONSTRAINT respuesta_pkey PRIMARY KEY (id_respuesta);

ALTER TABLE ONLY public.tipo_documento
    ADD CONSTRAINT tipo_documento_pk PRIMARY KEY (id_tipo_documento);

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pk PRIMARY KEY (id_usuario);

ALTER TABLE ONLY public.bitacora
    ADD CONSTRAINT accion_bitacora_fk FOREIGN KEY (id_accion) REFERENCES public.accion(id_accion);
ALTER TABLE ONLY public.asunto_area_universitaria
    ADD CONSTRAINT area_asunto_area FOREIGN KEY (id_area_universitaria_organizacion) REFERENCES public.area_universitaria_organizacion(id_area_universitaria_organizacion) NOT VALID;

ALTER TABLE ONLY public.asunto_relacionado
    ADD CONSTRAINT as2_asunto_relacionado_fk_1 FOREIGN KEY (id_asunto_relacionado) REFERENCES public.asunto(id_asunto);
COMMENT ON CONSTRAINT as2_asunto_relacionado_fk_1 ON public.asunto_relacionado IS 'asunto con el que se relaciona';

ALTER TABLE ONLY public.asunto_relacionado
    ADD CONSTRAINT as_asunto_relacionado_fk FOREIGN KEY (id_asunto) REFERENCES public.asunto(id_asunto);

ALTER TABLE ONLY public.asunto_area_universitaria
    ADD CONSTRAINT asunto_asunto_area_universitaria_fk FOREIGN KEY (id_asunto) REFERENCES public.asunto(id_asunto);

ALTER TABLE ONLY public.asunto_documento
    ADD CONSTRAINT asunto_asunto_documento_fk FOREIGN KEY (id_asunto) REFERENCES public.asunto(id_asunto);

ALTER TABLE ONLY public.asunto_persona
    ADD CONSTRAINT asunto_persona_fk FOREIGN KEY (id_asunto) REFERENCES public.asunto(id_asunto);

ALTER TABLE ONLY public.respuesta
    ADD CONSTRAINT asunto_respuesta_fk FOREIGN KEY (id_asunto) REFERENCES public.asunto(id_asunto);

ALTER TABLE ONLY public.asunto
    ADD CONSTRAINT estado_asunto_fk FOREIGN KEY (id_estado) REFERENCES public.estado(id_estado);

ALTER TABLE ONLY public.asunto
    ADD CONSTRAINT rector_asunto_fk FOREIGN KEY (id_gestion_rector) REFERENCES public.gestion_rector(id_gestion_rector);

ALTER TABLE ONLY public.respuesta_documento
    ADD CONSTRAINT resp_respuesta_documento_fk FOREIGN KEY (id_respuesta) REFERENCES public.respuesta(id_respuesta);

ALTER TABLE ONLY public.asunto_documento
    ADD CONSTRAINT tipo_docto_documento_fk FOREIGN KEY (id_tipo_documento) REFERENCES public.tipo_documento(id_tipo_documento);

ALTER TABLE ONLY public.respuesta
    ADD CONSTRAINT tipo_resp_respuesta_fk FOREIGN KEY (id_tipo_respuesta) REFERENCES public.tipo_respuesta(id_tipo_respuesta);

ALTER TABLE ONLY public.asunto_relacionado
    ADD CONSTRAINT usu_asunto_relacionado_fk FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);

ALTER TABLE ONLY public.asunto_documento
    ADD CONSTRAINT usuario_asunto_fk FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);

ALTER TABLE ONLY public.respuesta_documento
    ADD CONSTRAINT usuario_asunto_fk FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);

ALTER TABLE ONLY public.asunto
    ADD CONSTRAINT usuario_asunto_fk_1 FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);
ALTER TABLE ONLY public.bitacora
    ADD CONSTRAINT usuario_bitacora FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario) NOT VALID;


CREATE TABLE IF NOT EXISTS public.area_involucrado
(
    id_area_involucrado integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    nombre character varying(250) COLLATE pg_catalog."default" NOT NULL,
    activo boolean NOT NULL DEFAULT true,
    created_at timestamp without time zone NOT NULL DEFAULT now(),
    updated_at timestamp without time zone,
    CONSTRAINT area_involucrado_pkey PRIMARY KEY (id_area_involucrado),
    CONSTRAINT area_r_uk UNIQUE (nombre)
);

COMMENT ON TABLE public.area_involucrado
    IS 'Área universitaria, organización o institución que está involucrada en la respuesta.';

COMMENT ON COLUMN public.area_involucrado.id_area_involucrado
    IS 'Identificador del área universitaria o empresa.';

COMMENT ON COLUMN public.area_involucrado.nombre
    IS 'Nombre del área universitaria, empresa, organización o institución.';
	
COMMENT ON COLUMN public.area_involucrado.activo
    IS 'Indica si el involucrado se encuentra Activo = true, Inactivo = false.';	

COMMENT ON COLUMN public.area_involucrado.created_at
    IS 'Fecha y hora de creación del registro.';

COMMENT ON COLUMN public.area_involucrado.updated_at
    IS 'Fecha y hora de última modificación del registro.';
	
	
CREATE TABLE IF NOT EXISTS public.respuesta_area_involucrado
(
    id_respuesta_area_involucrado integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    id_respuesta integer NOT NULL,
    id_area_involucrado integer NOT NULL,
    created_at timestamp without time zone NOT NULL DEFAULT now(),
    updated_at timestamp without time zone,
    CONSTRAINT respuesta_area_pkey PRIMARY KEY (id_respuesta_area_involucrado),
    CONSTRAINT respuesta_area FOREIGN KEY (id_area_involucrado)
        REFERENCES public.area_involucrado (id_area_involucrado) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT respuesta_area_involucrado_fk FOREIGN KEY (id_respuesta)
        REFERENCES public.respuesta (id_respuesta) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
;

COMMENT ON TABLE public.respuesta_area_involucrado
    IS 'Nombres de las áreas universitarias, instituciones, empresas u organizaciones a las que pertenecen las personas que solicitan la atención de un asunto recibido en la Oficina de Rectoría.';

COMMENT ON COLUMN public.respuesta_area_involucrado.id_respuesta_area_involucrado
    IS 'Identificador consecutivo del asunto y la persona.';

COMMENT ON COLUMN public.respuesta_area_involucrado.id_respuesta
    IS 'Identificador consecutivo de la respuesta.';

COMMENT ON COLUMN public.respuesta_area_involucrado.id_area_involucrado
    IS 'Identificador de la empresa o área universitaria involucrada en la respuesta.';

COMMENT ON COLUMN public.respuesta_area_involucrado.created_at
    IS 'Fecha y hora de creación del registro.';

COMMENT ON COLUMN public.respuesta_area_involucrado.updated_at
    IS 'Fecha y hora de última modificación del registro.';	