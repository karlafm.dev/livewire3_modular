INSERT INTO tipo_documento(id_tipo_documento, nombre, nomenclatura, activo) OVERRIDING SYSTEM VALUE VALUES (1,'Convenio','convenio','true');
INSERT INTO tipo_documento(id_tipo_documento, nombre, nomenclatura, activo) OVERRIDING SYSTEM VALUE VALUES (2,'Correo','correo','true');
INSERT INTO tipo_documento(id_tipo_documento, nombre, nomenclatura, activo) OVERRIDING SYSTEM VALUE VALUES (3,'Folletos, Revistas o Libros','folleto','true');
INSERT INTO tipo_documento(id_tipo_documento, nombre, nomenclatura, activo) OVERRIDING SYSTEM VALUE VALUES (4,'Invitaciones','invitacion','true');
INSERT INTO tipo_documento(id_tipo_documento, nombre, nomenclatura, activo) OVERRIDING SYSTEM VALUE VALUES (5,'Oficios','oficio','true');
INSERT INTO tipo_documento(id_tipo_documento, nombre, nomenclatura, activo) OVERRIDING SYSTEM VALUE VALUES (6,'Oficios Firmados por el Rector','rector','true');
