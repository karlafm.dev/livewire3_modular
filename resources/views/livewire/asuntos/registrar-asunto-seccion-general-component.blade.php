<div>
    <x-dialog-modal wire:model="modalAbierto">
        <x-slot name="title">
            Asunto
        </x-slot>

        <x-slot name="content">
        <form wire:submit="save">
            <div>
                <label for="folio">Folio
                    <span class="">*</span>
                </label>
                <textarea class="" id="folio" rows="3" wire:model="form.folio"></textarea>
                @error('form.folio') <span class="">{{ $message }}</span> @enderror
            </div>
            <div>
                <label for="fecha_recepcion">Fecha de recepcion
                    <span class="">*</span>
                </label>
                <input type="date" class="" id="fecha_recepcion" wire:model="form.fecha_recepcion">
                @error('form.fecha_recepcion') <span class="">{{ $message }}</span> @enderror
            </div>
            <div >
                <label for="id_gestion_rector">Gestion rectoral
                    <span class="">*</span>
                </label>
                <select class="" id="id_gestion_rector" wire:model="form.id_gestion_rector">
                    @foreach ($this->gestionesRector as $gestion)
                    <option value="{{ $gestion->id_gestion_rector}}">{{ $gestion->nombre_periodo}}</option>
                    @endforeach
                </select>
                @error('form.id_gestion_rector') <span class="">{{ $message }}</span> @enderror
            </div>

            <div>
                <label for="asunto">Asunto
                    <span class="">*</span>
                </label>
                <textarea class="" id="asunto" rows="3" wire:model="form.asunto"></textarea>
                @error('form.asunto') <span class="">{{ $message }}</span> @enderror
            </div>
            <div>
                <label for="detalle">Detalle del asunto
                </label>
                <textarea class="" id="detalle" rows="3" wire:model="form.detalle"></textarea>
                @error('form.detalle') <span class="">{{ $message }}</span> @enderror
            </div>

            <div >
                <label for="id_estado">Estatus
                    <span class="">*</span>
                </label>
                <select class="" id="id_estado" wire:model="form.id_estado">
                    <option value="1">Registrado</option>
                    <option value="2">En atención</option>
                    <option value="3">Cerrado</option>
                </select>
                @error('form.id_estado') <span class="">{{ $message }}</span> @enderror
            </div>

            <button>Guardar</button>
        </form>
    </x-slot>

    <x-slot name="footer">
        <x-secondary-button wire:click="$toggle('confirmingUserDeletion')" wire:loading.attr="disabled">
            Nevermind
        </x-secondary-button>

        <x-button class="ml-2" wire:click="deleteUser" wire:loading.attr="disabled">
            Delete Account
        </x-button>
    </x-slot>
</x-dialog-modal>



</div>
