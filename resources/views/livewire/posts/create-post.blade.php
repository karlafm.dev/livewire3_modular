<div>
    <form wire:submit="guardar">
        <input type="text" wire:model="form.title" placeholder="Title">
        <div>
            @error('form.title') <span class="error">{{ $message }}</span> @enderror
        </div>

        <input type="text" wire:model="form.content" placeholder="Contents">
        <div>
            @error('form.content') <span class="error">{{ $message }}</span> @enderror
        </div>

        <button type="submit">Save</button>
        <button type="button" wire:click="resetForm()">Reset</button>
    </form>
</div>