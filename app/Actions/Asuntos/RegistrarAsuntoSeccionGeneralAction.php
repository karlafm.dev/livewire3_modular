<?php

namespace App\Actions\Asuntos;

use Exception;
use App\Models\Asunto;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Livewire\Forms\Asuntos\RegistrarAsuntoSeccionGeneralForm;

class RegistrarAsuntoSeccionGeneralAction
{
    public static function execute(RegistrarAsuntoSeccionGeneralForm $form, $usuarioId): void
    {

        try {
            $form->validate();

            DB::transaction(function () use ($form, $usuarioId) {

                $asunto = Asunto::updateOrCreate(
                    ['id_asunto' => $form->id_asunto ?? 0],
                    [
                        ...$form->all(),
                        'id_usuario' => $usuarioId,
                    ]
                );

                //Bitacora

            });
        } catch (Exception $e) {
            Log::error($e::class . ' > ' . $e->getFile() . '('.$e->getLine().'): ' . $e->getMessage());
            throw $e;
        }
    }
}
