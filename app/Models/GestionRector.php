<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\QueryBuilders\GestionRectorQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class GestionRector extends Model
{
    use HasFactory;

    protected $table = 'gestion_rector';
    protected $primaryKey = 'id_gestion_rector';

    public function newEloquentBuilder($query)
    {
        return new GestionRectorQueryBuilder($query);
    }
}
