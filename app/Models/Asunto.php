<?php

namespace App\Models;

use App\Enums\EstadoAsuntoEnum;
use App\Models\Estado;
use Illuminate\Database\Eloquent\Model;
use App\QueryBuilders\AsuntoQueryBuilder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Asunto extends Model
{
    use HasFactory;
    protected $table = 'asunto';
    protected $primaryKey = 'id_asunto';

    protected $fillable = [
        'id_estado', 'id_usuario', 'id_gestion_rector',
        'folio', 'asunto', 'detalle', 'fecha_recepcion'
    ];

    protected $casts = [
        'id_estado' => EstadoAsuntoEnum::class
    ];

    public function gestionRector() : BelongsTo
    {
        return $this->belongsTo(GestionRector::class, 'id_gestion_rector', 'id_gestion_rector');
    }

    public function estado() : BelongsTo
    {
        return $this->belongsTo(Estado::class, 'id_estado', 'id_estado');
    }

    public function newEloquentBuilder($query)
    {
        return new AsuntoQueryBuilder($query);
    }

}
