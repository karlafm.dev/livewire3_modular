<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateActionCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:action 
                            {name : The name of the Action class}';
                            

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Action class';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('name');
        $path = app_path("Actions/{$name}Action.php");

        if (file_exists($path)) {
            $this->error('Action already exists!');
            return false;
        }

        if (!is_dir($directory = app_path('Actions'))) {
            mkdir($directory, 0755, true);
        }
        file_put_contents($path, $this->getStub());


        $this->info('Action created successfully.');

    }

    /**
     * Get the stub file for the generator.
     */
    protected function getStub()
    {
        return '<?php

namespace App\Actions;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
        
class ' . $this->argument('name') . 'Action
{
    public static function execute($id, $usuarioId): void
    {
        try {
            DB::transaction(function () use ($id, $usuarioId) {
                
             });
        } catch (Exception $e) {
            Log::error($e::class . \' > \' . $e->getFile() . \'(\'.$e->getLine().\'): \' . $e->getMessage());
            throw $e;
        }
    }
}
';
    }


}
