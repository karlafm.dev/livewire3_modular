<?php

namespace App\Livewire\Forms\Asuntos;

use Livewire\Form;
use App\Models\Asunto;
use Livewire\Attributes\Rule;

class RegistrarAsuntoSeccionGeneralForm extends Form
{
    public ?int $id_asunto;
    public $id_estado = '';
    public $id_usuario = 1;
    public $id_gestion_rector = '';
    public $folio = '';
    public $asunto = '';
    public $detalle = '';
    public $fecha_recepcion = '';

    public function setAsunto($idAsunto)
    {
        $asunto = Asunto::findOrNew($idAsunto);

        if ($asunto === null){
            return;
        }

        $this->id_asunto = $asunto->id_asunto;
        $this->id_estado = $asunto->id_estado;
        $this->id_usuario = $asunto->id_usuario;
        $this->id_gestion_rector = $asunto->id_gestion_rector;
        $this->folio = $asunto->folio;
        $this->asunto = $asunto->asunto;
        $this->detalle = $asunto->detalle;
        $this->fecha_recepcion = $asunto->fecha_recepcion;
    }

    public function rules () : array
    {
        return [
            //'id_asunto' => ['required'],
            'id_estado' => ['required'],
            //'id_usuario' => ['required'],
            'folio' => ['required', 'max:8'],
            'asunto' => ['required'],
            'detalle' => ['required'],
            'fecha_recepcion' => ['required'],
        ];
    }
}
