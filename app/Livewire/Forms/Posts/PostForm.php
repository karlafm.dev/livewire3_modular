<?php

namespace App\Livewire\Forms\Posts;

use Livewire\Attributes\Rule;
use Livewire\Form;

class PostForm extends Form
{

    public $title = '';
    public $content = '';

    public function rules () : array
    {
        return [
            'title' => 'required',
            'content' => ['required']
        ];
    }
}
