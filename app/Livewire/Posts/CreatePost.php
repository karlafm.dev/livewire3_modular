<?php
namespace App\Livewire\Posts;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Livewire\Forms\Posts\PostForm;
use App\Actions\Posts\CreatePostAction;

class CreatePost extends Component
{
    public PostForm $form;

    public function render()
    {
        return view('livewire.posts.create-post');
    }

    public function guardar()
    {
        $this->validate();
        CreatePostAction::execute($this->form, Auth::id());

    }
}
