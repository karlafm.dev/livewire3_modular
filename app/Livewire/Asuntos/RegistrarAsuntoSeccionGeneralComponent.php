<?php

namespace App\Livewire\Asuntos;

use App\Models\Asunto;
use Livewire\Component;
use App\Models\GestionRector;
use App\Enums\EstadoAsuntoEnum;
use Livewire\Attributes\Computed;
use Illuminate\Support\Facades\Auth;
use App\Actions\Asuntos\RegistrarAsuntoSeccionGeneralAction;
use App\Livewire\Forms\Asuntos\RegistrarAsuntoSeccionGeneralForm;
use Livewire\Attributes\On;

class RegistrarAsuntoSeccionGeneralComponent extends Component
{

    public RegistrarAsuntoSeccionGeneralForm $form;
    public $modalAbierto = false;

    //protected $listeners = ['registrarAsunto'];

    public function mount(int $id_asunto = null)
    {
        $this->form->setAsunto($id_asunto);

        if($this->form->id_estado === EstadoAsuntoEnum::Cerrado) {
            //dump("ya esta cerrado, no se puede editar");
        }

        if ($this->form->id_asunto === null) {
            //dump("Es registro");
        }
    }

    #[On('registrar-asunto')]
    public function registrarAsunto () {
        $this->modalAbierto = true;
    }

    public function render()
    {
        return view('livewire.asuntos.registrar-asunto-seccion-general-component');
    }

    #[Computed]
    public function gestionesRector()
    {
        return GestionRector::listar();
    }

    public function save()
    {
        $this->validate();

        try {
            RegistrarAsuntoSeccionGeneralAction::execute($this->form, Auth::id());
        } catch (\Exception $e) {
            //Mensaje de error
        }

    }
}
