<?php

namespace App\Livewire\Asuntos;

use Livewire\Component;
use Livewire\Attributes\Computed;
use Illuminate\Support\Facades\DB;

class ListarPersonasComponent extends Component
{
    public function render()
    {
        return view('livewire.asuntos.listar-personas-component');
    }

    #[Computed]
    public function personas() {
        return DB::table('asunto')->get();
    }
}
