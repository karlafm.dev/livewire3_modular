<?php
namespace App\Enums;


enum EstadoAsuntoEnum : int {
    case Registrado = 1;
    case EnAtencion = 2;
    case Cerrado = 3;
}