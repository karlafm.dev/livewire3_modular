<?php
namespace App\QueryBuilders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;


class GestionRectorQueryBuilder extends Builder
{
    public function listar() {
        return $this->select ('id_gestion_rector', 'nombre_periodo')
        //->where('vigente', DB::raw('true'))
        ->orderBy('nombre_periodo')
        ->get();
    }
}